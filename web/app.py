from flask import Flask, render_template, request, send_from_directory

app = Flask(__name__)

@app.route("/")
def index():
    name = request.args.get("name")
    if name == None:
        name = "Index"
    return render_template("index.html"), name

@app.route('/<path:filename>')
def op_file(filename):
    checker = filename.split('/')
    request_file = checker[-1]
    rf_len_mod = 0 - len(request_file)
    if (filename[rf_len_mod-2:rf_len_mod] == '//') | (request_file[0:2] == '..') | (request_file[0] == '~'):
        return error_403(403)
    return send_from_directory('./pages/', filename), 200

@app.errorhandler(404)
def error_404(e):
    return render_template("404.html"), 404

@app.errorhandler(403)
def error_403(e):
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
